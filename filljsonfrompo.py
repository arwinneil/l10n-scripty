#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Copyright 2014  Burkhard Lück <lueck@hube-lueck.de>
Copyright 2016 Alex Richardson <arichardson.kde@gmail.com>

Permission to use, copy, modify, and distribute this software
and its documentation for any purpose and without fee is hereby
granted, provided that the above copyright notice appear in all
copies and that both that the copyright notice and this
permission notice and warranty disclaimer appear in supporting
documentation, and that the name of the author not be used in
advertising or publicity pertaining to distribution of the
software without specific, written prior permission.

The author disclaim all warranties with regard to this
software, including all implied warranties of merchantability
and fitness.  In no event shall the author be liable for any
special, indirect or consequential damages or any damages
whatsoever resulting from loss of use, data or profits, whether
in an action of contract, negligence or other tortious action,
arising out of or in connection with the use or performance of
this software.
"""

import sys, os, glob, polib, json, pprint, codecs, csv, io, traceback
from jsontranslationcommon import *

pofiledict = {}


def findTranslations(json, field, msgctxt):
  global pofiledict
  if not json.get(field):
    return json

  # remove the old translations before inserting new ones:
  for key in list(json.keys()):
    if key.startswith(field + '['):
      del json[key]

  msgid = json[field]
  islist = type(msgid) is list
  if islist:
    msgid = serializeList(msgid)
    debugPrint('Handling JSON list entry %s: %s' % (field, msgid))
  for lang in pofiledict:
    langkey = '%s[%s]' % (field, lang)
    for entry in pofiledict[lang].translated_entries():
      if entry.msgid != msgid:
        continue
      if entry.msgctxt != msgctxt:
        debugPrint('Found JSON translation with same id, but different context: "%s" vs "%s": "%s"' % (entry.msgctxt, msgctxt, msgid))
        continue
      if entry.msgstr == '':
        continue
      if islist:
        json[langkey] = deserializeList(entry.msgstr)
      else:
        json[langkey] = entry.msgstr
  return json


def handleKioProtocols(json, filename):
  protocols = json.get(kioprotocolsfield)
  if not protocols:
    return
  debugPrint('Handling KIO protocol (%s)' % filename)
  for name in list(protocols.keys()):
    debugPrint('Checking KIO protocol %s (%s)' % (name, filename))
    if protocols[name].get('ExtraNames'):
      debugPrint('Found KIO protocol %s (%s) with ExtraNames' % (name, filename))
    protocols[name] = findTranslations(protocols[name], 'ExtraNames', name + ' ExtraNames')
  # debugPrint(pprint.pformat(json))


def main():
  repodir, jsonfilename, l10ndirpath, l10nmodulename, pofilename = sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5]

  jsonpofilelist = glob.glob("%s/*/messages/%s/%s" %(l10ndirpath, l10nmodulename, pofilename))

  if not l10ndirpath.endswith("/"):
    l10ndirpath += "/"
  for pofile in jsonpofilelist:
    langcode = pofile.split(l10ndirpath)[1].split("/")[0]
    po = polib.pofile(pofile)
    pofiledict[langcode] = po

  debugPrint('Found translations for %s' % pofiledict.keys())

  with open(jsonfilename, 'r') as data_file:
    try:
      jsondata = json.load(data_file, encoding='utf-8')
    except json.JSONDecodeError as v:
      raise RuntimeError('%s seems to be malformed json: %s' % (jsonfilename, v.msg)) from None
    if not type(jsondata) is dict:
      debugPrint('Cannot fill translations since JSON file %s is not an object' % jsonfilename)
      return
    if kpluginfield not in jsondata and kioprotocolsfield not in jsondata:
      debugPrint('Skipping JSON file %s as it does not have a "%s" or a "%s" key' % (jsonfilename, kpluginfield, kioprotocolsfield))
      return
    else:
      try:

        extrajsontranslationkeys = []

        try:
          with open(repodir + "/extraJsonTranslationKeys.txt", "r") as f:
            extrajsontranslationkeys = [line.rstrip('\n') for line in f]
        except:
          pass

        alltranslationfields = translationfields + extrajsontranslationkeys


        if len(set(jsondata.get(kpluginfield).keys()).intersection(alltranslationfields)) == 0:
          debugPrint('Skipping JSON file %s as it does not have a translationfield' %jsonfilename)
          return
      except AttributeError:
          debugPrint('AttributeError: "NoneType" object has no attribute "keys", skipping JSON file %s' %jsonfilename)
          return
    if pofiledict == {}:
      debugPrint('no translations found')
      debugPrint('args: %s' % sys.argv)
    # verbosePrint('original:\n "%s"\n\n' % pprint.pformat(kplugin))
    handleKioProtocols(jsondata, os.path.basename(jsonfilename))
    kplugin = jsondata.get(kpluginfield)
    if kplugin:
      for field in alltranslationfields:
        findTranslations(kplugin, field, field)
      # now handle the KAboutPerson objects:
      for field in kaboutpersonfields:
        for obj in kplugin.get(field, []):
          findTranslations(obj, "Name", field + ' Name')
          findTranslations(obj, "Task", field + ' Task')

    # verbosePrint('final result:\n "%s"\n\n' % (pprint.pformat(jsondata)))
    with io.open(jsonfilename, 'w', encoding='utf-8') as data_file:
      output = json.dumps(jsondata, sort_keys=True, indent=4, ensure_ascii=False)
      output += '\n'
      data_file.write(output)


if __name__ == "__main__":
  if len(sys.argv) != 6:
    print('wrong number of args: %s' %sys.argv)
    print('\nUsage: python %s basedir path/to/jsonfile path/to/l10ndir/ l10nmodulename pofilename.po' %os.path.basename(sys.argv[0]))
  else:
    try:
      main()
    except:
      print('cannot process %s: %s' % (sys.argv[1], traceback.format_exc()))
      traceback.print_exc()
