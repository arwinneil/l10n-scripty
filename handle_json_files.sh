#! /bin/bash
# kate: space-indent on; indent-width 2; replace-tabs on;

get_prj_name() {
  local mod=$1
  echo ${mod} | cut -d'_' -f2
}

extract_json() {
  arg1=$1; shift
  dest=$1; shift
  python3 ${dir}/createjsoncontext.py "$@" "$arg1" > json.$$.tmp
  msguniq --to-code=UTF-8 -o json.$$ json.$$.tmp 2>/dev/null
  if test -f json.$$; then
    if test ! -f  "$dest"; then
      echo "File $dest is missing!"
      mv json.$$ "$dest"
    elif diff -q -I^\"POT-Creation-Date: json.$$ "$dest" > /dev/null; then
      rm -f json.$$
      touch "$dest"
    else
      mv json.$$ "$dest"
    fi
  fi
  rm -f json.$$ json.$$.tmp
}

dir=$(dirname "$0")
. "$dir/get_paths"
releases=$(list_modules "$dir")

for mod in $releases; do
  jsonfilelist=$(find "$BASEDIR/$(get_path "$mod")" -name "*.json" -o -name "*.json.cmake"  | grep -v '/autotests/' | grep -v '/tests/' | grep -v 'APPNAMELC' | sort)
  if test -n "$jsonfilelist"; then
    mod_po_path=$(get_po_path "$mod")
    prj_name=$(get_prj_name "$mod")

    repo_dir="$BASEDIR/$(get_path "$mod")"

    extract_json $repo_dir "templates/messages/${mod_po_path}/${prj_name}._json_.pot" $jsonfilelist
    for jsonfile in $jsonfilelist; do
      python3 ${dir}/filljsonfrompo.py $repo_dir "$jsonfile" "$L10NDIR" "${mod_po_path}" "${prj_name}._json_.po"
    done
  fi
done
