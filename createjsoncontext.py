#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright 2014  Burkhard Lück <lueck@hube-lueck.de>
Copyright 2016 Alex Richardson <arichardson.kde@gmail.com>

Permission to use, copy, modify, and distribute this software
and its documentation for any purpose and without fee is hereby
granted, provided that the above copyright notice appear in all
copies and that both that the copyright notice and this
permission notice and warranty disclaimer appear in supporting
documentation, and that the name of the author not be used in
advertising or publicity pertaining to distribution of the
software without specific, written prior permission.

The author disclaim all warranties with regard to this
software, including all implied warranties of merchantability
and fitness.  In no event shall the author be liable for any
special, indirect or consequential damages or any damages
whatsoever resulting from loss of use, data or profits, whether
in an action of contract, negligence or other tortious action,
arising out of or in connection with the use or performance of
this software.
"""

import sys, os, json, time, pprint, codecs
from jsontranslationcommon import *

# a map msgctxt -> (msgid, filename) to merge duplicate fields
translations = {}

def potheader():
  headertxt =  "#, fuzzy\n"
  headertxt += "msgid \"\"\n"
  headertxt += "msgstr \"\"\n"
  headertxt += "\"Project-Id-Version: json files\\n\"\n"
  headertxt += "\"Report-Msgid-Bugs-To: https://bugs.kde.org\\n\"\n"
  headertxt += "\"POT-Creation-Date: %s+0000\\n\"\n" %time.strftime("%Y-%m-%d %H:%M")
  headertxt += "\"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\\n\"\n"
  headertxt += "\"Last-Translator: FULL NAME <EMAIL@ADDRESS>\\n\"\n"
  headertxt += "\"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\\n\"\n"
  headertxt += "\"MIME-Version: 1.0\\n\"\n"
  headertxt += "\"Content-Type: text/plain; charset=UTF-8\\n\"\n"
  headertxt += "\"Content-Transfer-Encoding: 8bit\\n\"\n"
  headertxt += "\n"
  return headertxt


def addPotEntry(context, value, filename):
  global translations
  if not value:
    return
  # make sure multiline strings and strings containing quotes are escaped properly for the .pot file:
  msgid = value.replace('"', '\\"').replace('\n', '\\n"\n"')
  ids = translations.get(context, [])
  # debugPrint("context: " + context + " msgid: " + msgid + ", " + pprint.pformat(ids))
  for idx, val in enumerate(ids):
    if val[0] == msgid:
      debugPrint('Found identical entry: id="%s", ctx="%s"' % (msgid, context))
      ids[idx] = (msgid, val[1] + ' ' + filename)
      translations[context] = ids
      return
  ids.append((msgid, filename))
  translations[context] = ids


def handleKAboutPersonObject(context, json, filename):
  for author in json:
    # translatable keys are Name and Task (see KAboutPerson::fromJSON())
    addPotEntry(context + ' Name', author.get('Name'), filename)
    addPotEntry(context + ' Task', author.get('Task'), filename)


def handleKioProtocols(json, filename):
  if not json:
    return
  for key in json.keys():
    obj = json[key]
    names = obj.get('ExtraNames')
    if not names:
      continue
    addPotEntry(key + ' ExtraNames', serializeList(names), filename)


if len(sys.argv) < 3:
  print('wrong number of args: %s' % sys.argv)
  print('\nUsage: python %s jsonfilenamelist basedir' %os.path.basename(sys.argv[0]))
else:
  jsonfilenamelist = [os.path.abspath(filename) for filename in sys.argv[1:-1]]
  basedir = os.path.abspath(sys.argv[-1])

  extrajsontranslationkeys = []

  try:
    with open(basedir + "/extraJsonTranslationKeys.txt", "r") as f:
      extrajsontranslationkeys = [line.rstrip('\n') for line in f]
  except:
    pass

  alltranslationfields = translationfields + extrajsontranslationkeys

  pottxt = ""
  for jsonfilename in jsonfilenamelist:
    with open(jsonfilename, 'r') as data_file:
      error = False
      try:
        jsondatadict = json.load(data_file, encoding='utf-8', strict=False)
        filename = os.path.relpath(jsonfilename, basedir)
        # try handling KDE-KIO-Protocols
        handleKioProtocols(jsondatadict.get(kioprotocolsfield), filename)
        kplugin = jsondatadict.get(kpluginfield)
        if not kplugin:
          continue
        for field in alltranslationfields:
          addPotEntry(field, kplugin.get(field), filename)
        for field in kaboutpersonfields:
          if field in kplugin:
            handleKAboutPersonObject(field, kplugin[field], filename)
      except:
        debugPrint('json file %s unsupported by scripty: %s' % (jsonfilename, sys.exc_info()))
  if translations:
    pottxt = ''
    debugPrint('Found translations:\n' + pprint.pformat(translations))
    for (context, values) in translations.items():
      for entry in values:
        msgid = entry[0]
        filename = entry[1]
        pottxt += '#: %s\nmsgctxt "%s"\nmsgid "%s"\nmsgstr ""\n\n' % (filename, context, msgid)
    print(potheader() + pottxt)
