#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2010 Albert Astals Cid <aacid@kde.org>
# SPDX-FileCopyrightText: 2020 Pino Toscano <pino@kde.org>

import os
import sys
import polib


if len(sys.argv) != 3:
    print("Usage: %s POFILE PHPFILE" % (os.path.basename(sys.argv[0])))
    exit(1)

popath = sys.argv[1]
phppath = sys.argv[2]

po = polib.pofile(popath)

linefmt = '$text["%s"] = "%s";'
lines = []
lines.append("<?php")
for entry in po.translated_entries():
    line = linefmt % (polib.escape(entry.msgid), polib.escape(entry.msgstr))
    lines.append(line)
lines.append("?>")
lines.append("\n")

ofh = open(phppath, "w")
ofh.write("\n".join(lines))
ofh.close()
